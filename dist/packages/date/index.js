"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __importDefault(require("@logger/core"));
exports.default = (function (t) {
    var now = new Date();
    core_1.default("[" + now + "]:" + t);
});
