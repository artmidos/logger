
import logger from '@logger/core';

export default (t: string) => {
  const now = new Date()
  logger(`[${now}]:` + t)
}